# plugin d'import csv pour Translucide

## liste des fichiers
- `plugin/import-csv/build.php` : il s'agit du coeur du traitement d'import csv. Ce fichier est à include dans `fiches-liste.php` afin de pouvoir réaliser l'import.
- `tpl/fiche.php` : une fiche correspondant à une ligne du fichier csv
- `tpl/fiches-liste.php` : liste des fiches provenant du fichier csv. C'est depuis ce modèle qu'on appelle le plugin pour importer le fichier csv
- `defaut.xlsx` : structure par défaut du tableur servant à l'import csv. Ce fichier est à exporter en `.csv`