/**
 * multilingue
 */
add_translation({
    "Upload file": {"fr" : "Téléverser un fichier"},
    "Delete": {"fr" : "Supprimer les "},
    "Valid file upload" : {"fr" : "Valider le téléversement"},
    "Please confirm deletion" : {"fr" : "Veuillez confirmer la suppression"},
    "Upload pending" : {"fr" : "Téléversement en cours"},
    "Upload successfull" : {"fr" : "Téléversement terminé"},
});

/**
 * ajout des boutons dans la barre d'admin
 */
html='<button id="del-'+tplEnfant+'s" class="mat small o50 ho1 ""><i class="fa fa-fw fa-trash big vam"></i><span class="noss vam">' + __('Delete') + tplEnfant + 's</span></button>'
    +'<form id="televerse" class="inbl">'
    +'<label id="upload" for="fichier" class="bt fl mat small t5 popin" aria-label="' + __("Upload file") + '"><i class="fa fa-upload  big vam"></i> <span class="vam no-small-screen">' + __("Upload file") + '</span></label>'
    +'<input id="fichier" name="fichier" type="file" accept=".csv" required hidden>'
    +'<input type="submit" hidden>'
    +'</form>'
$("#admin-bar").append(html);

/**
 *  évènements
 */



/** selection d'un fichier csv */
document.querySelector("#fichier").addEventListener("change", function(event) {

    event.preventDefault();
    if (confirm(__("Valid file upload"))) {

        document.querySelector("#televerse [type='submit']").click();

        //work in progress
        document.querySelector("#televerse #upload i").classList.remove("fa-upload");
        document.querySelector("#televerse #upload i").classList.add("fa-cog", "fa-spin");
        light("" + __("Upload pending") + " <i class='fa fa-cog fa-spin mlt'></i>");

    }

});

/** traitement d'un fichier csv */
document.querySelector("#televerse").addEventListener("submit", function(event) {
    
    event.preventDefault();

    //collecte du fichier
    const files = document.querySelector('[name=fichier]').files;

    // préparation des données
    const formData = new FormData(televerse);

    // envoi de la requête
    const xhr = new XMLHttpRequest();
    
    xhr.open('POST', path + "theme/" + theme + (theme ? "/" : "") + "plugin/import-csv/build.php?tpl=" + tpl + "&mode=televerse&permalink=" + permalink);
    
    xhr.onload = function() {
        
        const response = document.createRange().createContextualFragment(this.response);
        document.body.append(response);

        /** mise à jour du bouton */
        document.querySelector("#televerse #upload i").classList.remove("fa-cog","fa-spin");
        document.querySelector("#televerse #upload i").classList.add("fa-ok");
        document.querySelector("#televerse #upload").classList.add("saved");

        /** affichage du message de succès */
        light(__('Upload successfull'), 1000);
        
    }
    
    xhr.send(formData);
    
});
//-- fin de script téléversement



/*
// script 


/** suppressions enregistrement */
document.querySelector('#del-'+tplEnfant+'s').addEventListener("click", function(event) {
    
    event.preventDefault();

    // on demande la validation pour sécuriser la suppression des données
    if (confirm(__("Please confirm deletion"))) {

        // envoi de la requête
        const xhr = new XMLHttpRequest();

        xhr.open('POST', path + "theme/" + theme + (theme ? "/" : "") + "plugin/import-csv/build.php?tpl=" + tpl + "&mode=delete&permalink=" + permalink);

        xhr.onload = function() {

            const response = document.createRange().createContextualFragment(this.response);
            document.body.append(response);

            /** Mise à jour du bouton */
            document.querySelector('#del-'+tplEnfant+'s i').classList.remove("fa-trash");
            document.querySelector('#del-'+tplEnfant+'s i').classList.add("fa-ok");
            document.querySelector('#del-'+tplEnfant+'s i').classList.add("saved"); 

        }

        xhr.send();

    }
});