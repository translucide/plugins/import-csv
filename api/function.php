<?php

// Fonction pour téléverser le fichier CSV
function televerserFichierCsv($tplEnfant, $filtres, $tpl, $files)
{

    echo 'televerse';
    $_SESSION['filtres'] = $_POST;
    $_SESSION['tpl'] = $_GET['tpl'];

    // LECTURE DU FICHIER CSV
    $csv = array_map('str_getcsv', file($_FILES['fichier']['tmp_name']));
    $i = 0;

    // on commence une transaction
	$connect = $GLOBALS['connect'];

    $tm = "tl_meta";
    $tc = "tl_content";
    $tt = "tl_tag";
    $handle = fopen($_FILES['fichier']['tmp_name'], "r");

    $GLOBALS['connect']->begin_transaction();
    $connect->autocommit(false);

    if(($handle = fopen($_FILES['fichier']['tmp_name'], "r")) !== FALSE) {
        
        while (($data = fgetcsv($handle, 0, ',')) !== FALSE) {

            /* ENTÊTE DU CSV */
            if($i == 0) {

                $colonne = 'a';
                $colonnes = array();
                $nb_colonnes = count($data);

                $j = 0;
                while($j <= $nb_colonnes) {

                    $colonnes = array_merge($colonnes, [$colonne]);
                    ++$colonne;
                    ++$j;

                }

                //on sauvegarde les libelle de l'entête
                $entete = array_combine($colonnes, array_merge($data, ['']));

                /*var_dump($entete);*/

                //on enregistre l'entête dans la table meta
                $id = 0;
                $type = 'csv-'.$tplEnfant;
                $cle = $_SESSION['lang'];
                $val = json_encode($entete, JSON_UNESCAPED_UNICODE);
                $ordre = 0;

                $sql_meta = "REPLACE INTO ".$tm." (id, type, cle, val, ordre) VALUES (?, ?, ?, ?, ?)";
                $stmt_meta = $connect->prepare($sql_meta);
                $stmt_meta->bind_param("isssi",$id, $type, $cle, $val, $ordre);
                    
                if($stmt_meta->error){// Si il y a une erreur
                    $GLOBALS['connect']->rollback(); // on annule la transaction
                    echo htmlspecialchars($stmt_meta)."\n<script>error(\"".htmlspecialchars($stmt_meta->error)."\");</script>";
                }
                else
                    $stmt_meta->execute();
            }
            /* LIGNES DU CSV */
            else {

                // on combine les lettres des colonnes avec les valeurs de la ligne
                (count($data)<count($colonnes) ? array_push($data,['']) : '');
                $content_fiche = array_combine($colonnes, array_slice($data, 0, count($colonnes)));

                // on map les données entre le csv et la bdd
                if(!empty($content_fiche[$_SESSION['mapping_csv']['state']])) {
                    $state = 'active'; 
                }else 
                    $state = 'deactivate';

                $url = '';
                foreach(@$_SESSION['mapping_csv']['url'] as $lettre){
                    $url .= str_replace(['\r', '\n', chr(13), chr(10), ' • ', ' · '], ' ', $content_fiche[$lettre]).' ';
                }
                $url = make_url($url);

                $lang = $_SESSION['lang'];
                $robots = 'noindex,nofollow';
                $type = $tplEnfant;
                $tpl = $tplEnfant;

                $title = '';
                $prev_lettre = 'a';
                foreach(@$_SESSION['mapping_csv']['title'] as $lettre) {

                    //gestion des séparateurs pour le title
                    if(!in_array($lettre,['-','—'])) {
                        $title .= str_replace(['\r', '\n', chr(13), chr(10)], ' ', $content_fiche[$lettre]).' ';
                    }
                    else {
                        if(!empty($content_fiche[$prev_lettre])) {
                            $title .= ' '.$lettre.' ';
                        }
                    }

                    $prev_lettre = $lettre;
                    
                }

                $description = str_replace(['\r', '\n', chr(13), chr(10)], ' ', @$content_fiche[$_SESSION['mapping_csv']['description']]).' ';

                $content_fiche = array_merge($content_fiche, ['liste-href' => $_GET['permalink']]);
                foreach(@$_SESSION['mapping_csv']['tag'] as $key => $lettre) 
                    $content_fiche = array_merge($content_fiche, [ $tplEnfant.'-libelle-'.++$key => $entete[$lettre] ]);
                
                if(@$_SESSION['mapping_csv']['content']) {

                    foreach(@$_SESSION['mapping_csv']['content'] as $key => $val) {

                        // si c'est un tableau (sources actions), on créer une liste
                        if(is_array($val)) {
                            $content = '<ul>';
                            foreach($val as $keyval => $lettre) {
                                if(preg_match("/(http|https):\/\//", $content_fiche[$lettre])) {
                                    $content.= '<li><a href="'.make_url($content_fiche[$lettre]).'" target="_blank">'.$content_fiche[$lettre].'</a></li>';
                                }
                            }
                            $content.='</ul>';
                            $content_fiche = array_merge($content_fiche, [ $key => $content ]);
                        }
                        else {
                            //si le champs est un lien
                            $lettre = $val;
                            if(preg_match("/(http|https):\/\//", $content_fiche[$lettre])) {
                                $content = '<ul><li><a href="'.make_url($content_fiche[$lettre]).'" target="_blank">'.$content_fiche[$lettre].'</a></li></ul>';
                                //print $url;
                                $content_fiche = array_merge($content_fiche, [ $key => $content ]);
                            }
                            else {
                                $content = '<p>'.$content_fiche[$lettre].'</p>';
                                $content_fiche = array_merge($content_fiche, [ $key => $content ]);
                            }
                        }
                        

                    }

                }

                $content = json_encode($content_fiche, JSON_UNESCAPED_UNICODE);

                $user_insert = (int)$_SESSION['uid'];
                $date_insert = date('Y-m-d H:i:s');
                if(!empty($_SESSION['mapping_csv']['date_insert'])) {
                    $date = @explode('-',$content_fiche[$_SESSION['mapping_csv']['date_insert']]);
                    $date_insert = @date('Y-m-d H:i:s',strtotime($date[1].'-'.$date[0].'-'.$date[2]));
                } 
                $user_update = (int)$_SESSION['uid'];
                $date_update = date('Y-m-d H:i:s');

                //debug
                /*print '<br><code>'
                //.''. $lang .' / '
                //.''. $robots .' / '
                //.''. $type .' / '
                //.''. $tpl .' / '
                .''. $url .' / '
                //.''. $title .' / '
                //.''. $content .' / '
                //.''. $user_insert .' / '
                //.''. $date_insert .' / '
                //.''. $user_update .' / '
                //.''. $date_update .' / '
                .$state.'</code><br>';*/

                // on commence une transaction
                $GLOBALS['connect']->begin_transaction();

                
                // on regarde si la fiche existe déjà en bdd
                $sql_existe = "SELECT SQL_CALC_FOUND_ROWS  ".$tc.".id from ".$tc." WHERE type='".$tplEnfant."' AND url='".$url."'";
                $sel_existe = $GLOBALS['connect']->query($sql_existe);
                $num_total = $GLOBALS['connect']->query("SELECT FOUND_ROWS()")->fetch_row()[0];
                    
                // Prépatation de la requête
                if($num_total > 0 ) {
                    $id = @$sel_existe->fetch_assoc()['id'];
                    /*$sql_fiche = "REPLACE INTO ".$tc." 
                    (id, state, title, description, tpl, url, lang, robots, type, content, user_insert, date_insert, user_update, date_update)
                    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";*/
                    $sql_fiche = "UPDATE ".$tc. " 
                    SET state=?,title=?,description=?,tpl=?,url=?,lang=?,robots=?,type=?,content=?,user_insert=?,date_insert=?,user_update=?,date_update=?
                    WHERE id=".$id;
                    $stmt_fiche = $GLOBALS['connect']->prepare($sql_fiche);
                    $stmt_fiche->bind_param("sssssssssisis", $state, $title, $description, $tpl, $url, $lang, $robots, $type, $content, $user_insert, $date_insert, $user_update, $date_update);
                } 
                else {
                    $sql_fiche = "INSERT INTO ".$tc." 
                    (state, title, description, tpl, url, lang, robots, type, content, user_insert, date_insert, user_update, date_update)
                    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                    $stmt_fiche = $GLOBALS['connect']->prepare($sql_fiche);
                    $stmt_fiche->bind_param("sssssssssisis", $state, $title, $description, $tpl, $url, $lang, $robots, $type, $content, $user_insert, $date_insert, $user_update, $date_update);
                }

                if($stmt_fiche->error){// Si il y a une erreur
                    $GLOBALS['connect']->rollback(); // on annule la transaction
                    echo htmlspecialchars($sql_fiche)."\n<script>error(\"".htmlspecialchars($stmt_fiche->error)."\");</script>";                   
                
                    $stmt_fiche->execute();
                    $connect->commit();
                }

                // on commence une transaction
                $GLOBALS['connect']->begin_transaction();

                        // on alimente les tag de la fiche s'il s'agit d'une fiche active
                        if($state == 'active') {

                            //récupération de l'id traité
                            $id = $stmt_fiche->insert_id; 

                            // on parcours les colonnes de tag
                            // @ todo: si plusieur tag dans la zone, spliter et ecrire en bdd (avec boucle)
                            foreach(@$_SESSION['mapping_csv']['tag'] as $key => $lettre) {

                                ++$key;

                                // ecriture dans la table meta des libelle de tag
                                $type = $tplEnfant.'-libelle-'.$key;
                                $cle = $entete[$lettre];
                                $ordre = $key;

                                $sql_meta = "REPLACE INTO ".$tm." (id, type, cle, ordre) VALUES (?, ?, ?, ?)";
                                $stmt_meta = $connect->prepare($sql_meta);
                                $stmt_meta->bind_param("issi",$id,$type,$cle,$ordre);
                                    
                                if($stmt_meta->error){// Si il y a une erreur
                                    $GLOBALS['connect']->rollback(); // on annule la transaction
                                    echo htmlspecialchars($stmt_meta)."\n<script>error(\"".htmlspecialchars($stmt_meta->error)."\");</script>";
                                } 
                                else
                                    $stmt_meta->execute();

                                // on commence une transaction
                                $GLOBALS['connect']->begin_transaction();

                                // si plusieur tag, on split et on boucle
                                $tags = explode(',',$content_fiche[$lettre]);

                                foreach($tags as $order => $tag) {

                                    $zone = $tplEnfant.'-tag-'.$key;
                                    $lang = $_SESSION['lang'];
                                    $encode = make_url($tag);
                                    $name = $tag;
                                    $ordre = $order;

                                    $sql_tag = "REPLACE INTO ".$tt." (id, zone, encode, name, ordre) VALUES (?, ?, ?, ?, ?)";
                                    $stmt_tag = $connect->prepare($sql_tag);
                                    $stmt_tag->bind_param("isssi",$id,$zone,$encode,$name,$ordre);

                                    if($stmt_tag->error){// Si il y a une erreur
                                        $GLOBALS['connect']->rollback(); // on annule la transaction
                                        echo htmlspecialchars($stmt_tag)."\n<script>error(\"".htmlspecialchars($stmt_tag->error)."\");</script>";
                                    } 
                                    else
                                        $stmt_tag->execute();

                                }
                            }
                        }
            }
            // $connect->commit();  
            
            ++$i;
        }
    }     
    
    fclose($handle);

    unset($_SESSION['mapping_csv']);

}

function deleteListe($tplEnfant){

    $sql = "DELETE FROM ".$tc." WHERE ".$tc.".tpl = '".$tplEnfant."'; ";     
    $GLOBALS['connect']->query($sql);

    $sql = "DELETE FROM ".$tm." WHERE ".$tm.".type LIKE '".$tplEnfant."%'; ";    
    $GLOBALS['connect']->query($sql);

    $sql = "DELETE FROM ".$tt." WHERE ".$tt.".zone LIKE '".$tplEnfant."%'; ";    
    $GLOBALS['connect']->query($sql);
}

?>