<?php
// liste des fiches
// @todo : documentation de ce template (à dupliquer dans les dossier de template du thème)

// @todo :  mettre le mapping des données ici
$GLOBALS['mapping_csv'] = array(
    'state' => 'a',
    //'lang' => [],
    'date_insert' => 'l',
    'date_update' => 'm',
    'url' => ['b'], //info: il s'agit d'un tableau si on souhaite concatener plusier champ
    'title' => ['c'], //info: il s'agit d'un tableau si on souhaite concatener plusier champ
    'description' => 'd',
    'tag' => ['i','j','k'],
    'content'=>['e','f','g']

);

//configuration des éléments à afficher sur la page
$GLOBALS['content_fiche'] = array (

    'header' => [

    ],

    'content' => [
        
        [
            //'f' => ['fa' => 'fa-location'],
            'e' => ['fa' => 'fa-tag'], //type de structure
            'f' => ['fa' => 'fa-star'], //esthétique
            'g' => ['fa' => 'fa-home',] //ressources mutualisées
        ],

    ],
    
);


?>